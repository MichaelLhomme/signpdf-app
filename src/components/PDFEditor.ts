import { PDFDocument, PDFImage } from 'pdf-lib'
import { Intersection } from './Signature'

// Private helper to load a file as an ArrayBuffer
function loadFile(file: File): Promise<ArrayBuffer> {
  return new Promise((resolve, _reject) => {
    const reader = new FileReader()
    reader.onloadend = (_e) => resolve(reader.result as ArrayBuffer)
    reader.readAsArrayBuffer(file);
  })
}

// Create a PDFDocument instance
export const createEditor = async (file: File): Promise<PDFDocument> => {
  const arrayBuffer = await loadFile(file)
  return await PDFDocument.load(arrayBuffer)
}

// Save the resulting document
export const saveAndDownload = async (doc: PDFDocument) => {
  const pdfBytes = await doc.save()
  const blob = new Blob([pdfBytes], {type: 'application/pdf'});
  const link = document.createElement('a');
  link.href = window.URL.createObjectURL(blob);
  const fileName = 'coucou.pdf';
  link.download = fileName;
  link.click();
}

// Compute coordinates and draw the signature
export const drawSignature = (doc: PDFDocument, intersection: Intersection, pngImage: PDFImage) => {
  const page = doc.getPages()[intersection.pageIndex]

  // Global ratio between page on screen and page on pdf
  const pageSize = page.getSize()
  const displayToPdfRatio = pageSize.width / intersection.boxWidth

  // Load and scale png to preserve aspect ratio
  const toPdfPageWidthRatio = pngImage.width / pageSize.width
  const pngSize = pngImage.scale(intersection.toPageWidthRatio / toPdfPageWidthRatio)

  // Draw the signature (coordinates are inversed, origin is top-left on display and bottom-left on PDF)
  page.drawImage(pngImage, {
    x: intersection.x * displayToPdfRatio,
    y: ((intersection.y * displayToPdfRatio) - (pageSize.height - pngSize.height)) * -1,
    width: pngSize.width,
    height: pngSize.height,
  })
}
