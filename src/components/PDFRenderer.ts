import * as pdfjsLib from 'pdfjs-dist'
import 'pdfjs-dist/build/pdf.worker.entry' // not needed since v1.9.1

import { ref, shallowRef, watch } from 'vue'


// Interface containing information about a page
export interface PageInfo {
  index: number // !!! WARNING !!! Start at 1
  page: HTMLCanvasElement | null
  preview: HTMLCanvasElement | null
}


// Rendering entrypoint
const renderDocument = (doc: pdfjsLib.PDFDocumentProxy, pagesInfo: PageInfo[], pageScale: number, previewScale: number): void => {
  console.debug('Rendering document')
  pagesInfo.forEach(info => {
    doc.getPage(info.index)
      .then((page: pdfjsLib.PDFPageProxy) => renderPage(page, info, pageScale, previewScale))
      .catch(error => console.error(`Error loading page ${info.index}`, error))
  })
}


// Render a page into its page canvas then copy into the preview
const renderPage = (page: pdfjsLib.PDFPageProxy, pageInfo: PageInfo, pageScale: number, previewScale: number): void => {
  console.debug(`Rendering page ${pageInfo.index}`)

  // Safety check
  if(!pageInfo.page) {
    console.warn(`No rendering canvas for page ${pageInfo.index}, skipping...`)
    return
  }

  // Prepare canvas using PDF page dimensions
  const viewport = page.getViewport({scale: pageScale})
  const canvas = pageInfo.page
  canvas.height = viewport.height
  canvas.width = viewport.width

  // Render PDF page into canvas context
  const canvasContext = canvas.getContext('2d')
  if(!canvasContext) throw 'unable to retrieve context from canvas'
  const renderTask = page.render({ canvasContext, viewport })
  renderTask.promise
    .then(() => { if(pageInfo.preview) copyPreview(pageInfo, pageScale, previewScale) })
    .catch(e => console.error(`Error rendering PDF page ${pageInfo.index}`, e))
}


// Copy the page canvas into its preview canvas
const copyPreview = (pageInfo: PageInfo, pageScale: number, previewScale: number): void => {
  if(!pageInfo.page || !pageInfo.preview) return

  // Compute a corrected scale as page width is used as a reference and is itself scaled
  const correctedScale = previewScale / pageScale

  // Prepare canvas using preview dimensions
  const canvas = pageInfo.preview
  canvas.width = pageInfo.page.width * correctedScale
  canvas.height = pageInfo.page.height * correctedScale

  const context = canvas.getContext('2d')
  if(!context) throw 'unable to retrieve context from canvas'
  context.drawImage(pageInfo.page, 0, 0, canvas.width, canvas.height);
}


/*
 * Setup PDFRenderer and returns refs and hooks
 */
export const usePDFRenderer = (initialPageScale: number, initialPreviewScale: number) => {
  // Scaling
  const pageScale = ref(initialPageScale)
  const previewScale = ref(initialPreviewScale)

  // Ref to doc and pages
  const pages = ref<PageInfo[]>([])
  const doc = shallowRef<pdfjsLib.PDFDocumentProxy>()

  // Page and preview registration callback
  const registerPageCanvas = (info: PageInfo, el: HTMLCanvasElement) => pages.value[info.index - 1].page = el
  const registerPreviewCanvas = (info: PageInfo, el: HTMLCanvasElement) => pages.value[info.index - 1].preview = el

  // Update pages on document update
  watch(doc, (newDoc) => {
    if(newDoc) {
      const newPages: PageInfo[] = []
      for(let i = 1; i <= newDoc.numPages; i++) { // !!! WARNING !!! Start at 1
        newPages.push({ index: i, page: null, preview: null })
      }
      pages.value = newPages
    } else {
      pages.value = []
    }
  })

  // Render document on the fly when **pages** are updated so the parent component has
  // time to update its templates and register pages and previews canvas (TODO render on registration ?)
  watch(pages, (newPages) => { if(newPages && newPages.length > 0 && doc.value) renderDocument(doc.value, newPages, pageScale.value, previewScale.value) })

  // Redraw on pageScale change
  watch(pageScale, (newPageScale) => { doc.value && renderDocument(doc.value, pages.value, newPageScale, previewScale.value) })

  // Redraw on previewScale change
  watch(previewScale, (newPreviewScale) => { doc.value && renderDocument(doc.value, pages.value, pageScale.value, newPreviewScale) })

  // Load a PDF document
  const loadDocument = (url: string) => {
    const loadingTask = pdfjsLib.getDocument(url)
    loadingTask.onProgress = () => console.log('pdf load progress')
    loadingTask.promise
      .then(pdf => doc.value = pdf)
      .catch(reason => console.error('PDF loading error', reason))
  }

  // Return refs and hooks
  return {
    doc,
    pages,
    pageScale,
    previewScale,
    loadDocument,
    registerPageCanvas,
    registerPreviewCanvas
  }
}
