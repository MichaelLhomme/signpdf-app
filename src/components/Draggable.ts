import { h, computed, ref, Ref } from 'vue'

// Export DragHandle here for convenience
import DragHandle from './DragHandle.vue'
export { DragHandle }

// Export DragHandles here for convenience
import DragHandles from './DragHandles.vue'
export { DragHandles }


// Type for current drag action
export enum DragAction {
  None = 1,
  Move,
  ResizeSouth,
  ResizeSouthEast,
  ResizeSouthWest,
  ResizeNorth,
  ResizeNorthEast,
  ResizeNorthWest,
  ResizeEast,
  ResizeWest
}


// Type for coordinates
export interface Coordinates {
  x: number
  y: number
}


// Type for sizes
export interface Size {
  width: number
  height: number
}


// Type for distances
export interface Distances {
  distanceX: number
  distanceY: number
}


// Type for bounding rect
export interface BoundingRect {
  top: number
  right: number
  bottom: number
  left: number
}


// Type for drag state
export interface DragState {
  action: DragAction
  distanceToAnchor: Distances
}


// Type for translate coordinates, values can be undefined when used for fixes when enforcing constraints
interface TranslateCoordinates {
  x?: number
  y?: number
}



// Helper function to translate mouse coordinates into the DOM parent referential
const toParentCoordinates = (event: MouseEvent, ref: HTMLElement): Coordinates => {
  if(!ref.parentNode) throw new Error('Cannot compute coordinates, no parent found')
  const parentNode: HTMLElement = ref.parentNode as HTMLElement
  const rect: DOMRect = parentNode.getBoundingClientRect()
  return { x: event.x - rect.x, y: event.y - rect.y }
}



// Translate a rect to a new position
const translateRectTo = (rect: BoundingRect, x: number, y: number): BoundingRect => {
  const offsetX = rect.left - x
  const offsetY = rect.top - y

  return {
    left: rect.left - offsetX,
    right: rect.right - offsetX,
    top: rect.top - offsetY,
    bottom: rect.bottom - offsetY
  }
}



// Compute the distance between a click and an anchor in the given bounding rect
const distanceToAnchor = (rect: BoundingRect, action: DragAction, { x, y }: Coordinates): Distances => {
  switch(action) {
  case DragAction.Move: return { distanceX: x - rect.left, distanceY: y - rect.top }
  case DragAction.ResizeEast: return { distanceX: x - rect.left, distanceY: 0 }
  case DragAction.ResizeWest: return { distanceX: x - rect.right, distanceY: 0 }
  case DragAction.ResizeNorth: return { distanceX: 0, distanceY: y - rect.top }
  case DragAction.ResizeSouth: return { distanceX: 0, distanceY: y - rect.bottom }
  case DragAction.ResizeNorthWest: return { distanceX: x - rect.left, distanceY: y - rect.top }
  case DragAction.ResizeNorthEast: return { distanceX: x - rect.right, distanceY: y - rect.top }
  case DragAction.ResizeSouthEast: return { distanceX: x - rect.right, distanceY: y - rect.bottom }
  case DragAction.ResizeSouthWest: return { distanceX: x - rect.left, distanceY: y - rect.bottom }
  default: throw new Error(`unknown drag action '${action}'`)
  }
}


// Helper to use a default value if the first parameter is undefined
const _valueOrDefault = (n: number | undefined, d: number) => n !== undefined ? n : d

// Helper to enforce a minimum to _valueOrDefault
const _valueOrDefaultOrMin = (n: number | undefined, d: number, min: number) => {
  const res = _valueOrDefault(n, d)
  return res < min ? min : res
}

// Helper to enforce a maximum to _valueOrDefault
const _valueOrDefaultOrMax = (n: number | undefined, d: number, max: number) => {
  const res = _valueOrDefault(n, d)
  return res > max ? max : res
}

// Translate the bounding rect anchor identified by action to the given point (whole rect when moving)
const translateAnchorTo = (rect: BoundingRect, action: DragAction, { x, y }: TranslateCoordinates): BoundingRect => {
  const v = _valueOrDefault
  const vMin = _valueOrDefaultOrMin
  const vMax = _valueOrDefaultOrMax

  switch(action) {
  case DragAction.Move: return translateRectTo(rect, v(x, rect.left), v(y, rect.top))
  case DragAction.ResizeNorthEast: return { ...rect, right: vMin(x, rect.right, rect.left), top: vMax(y, rect.top, rect.bottom) }
  case DragAction.ResizeNorthWest: return { ...rect, left: vMax(x, rect.left, rect.right), top: vMax(y, rect.top, rect.bottom) }
  case DragAction.ResizeSouthEast: return { ...rect, right: vMin(x, rect.right, rect.left), bottom: vMin(y, rect.bottom, rect.top) }
  case DragAction.ResizeSouthWest: return { ...rect, left: vMax(x, rect.left, rect.right), bottom: vMin(y, rect.bottom, rect.top) }
  default: throw new Error(`unknown drag action '${action}'`)
  }
}



/*
 * Base elements for draggable
 */
const baseDraggable = (domElement: Ref<HTMLElement>, { x, y }: Coordinates, { width, height }: Size) => {
  // separate size from position to avoid redraw on move
  const size = ref<Size>({ width, height })
  const rect = ref<BoundingRect>({
    left: x,
    top: y,
    right: x + width,
    bottom: y + height
  })

  const dragStyle = computed(() => ({ top: `${rect.value.top}px`, left: `${rect.value.left}px` }))
  const dragClasses = computed(() => ({ moveable: true, moving: isActionRunning() }))

  const dragState = ref<DragState>({
    action: DragAction.None,
    distanceToAnchor: { distanceX: 0, distanceY: 0 } as Distances
  })

  const isActionRunning = () => dragState.value.action !== DragAction.None
  const testShowAction = (otherAction: DragAction) => dragState.value.action === DragAction.None || dragState.value.action === otherAction

  const onDragStarted = (dragAction: DragAction, event: MouseEvent) => {
    dragState.value = {
      action: dragAction,
      distanceToAnchor: distanceToAnchor(rect.value, dragAction, toParentCoordinates(event, domElement.value))
    }
  }

  const onDragStopped = (_event: MouseEvent) => dragState.value.action = DragAction.None

  const updateRect = (updatedRect: BoundingRect) => {
    // Do not recreate a size object here but update the existing one
    // to prevents lag when moving (image is redrawn when size is updated).
    // This way Vue can detect the values are not updated and leave the DOM attributes as they are
    size.value.width = updatedRect.right - updatedRect.left
    size.value.height = updatedRect.bottom - updatedRect.top

    rect.value = updatedRect
  }

  return {
    size,
    rect,
    dragStyle,
    dragClasses,
    dragState,
    isActionRunning,
    testShowAction,
    onDragStarted,
    onDragStopped,
    updateRect
  }
}


/**
 * Setup draggable on the given element using initial position and size
 */
export const useDraggable = (domElement: Ref<HTMLElement>, initialPosition: Coordinates, initialSize: Size) => {
  const draggable = baseDraggable(domElement, initialPosition, initialSize)
  const { rect, size, dragState, onDragStarted, onDragStopped, updateRect } = draggable

  // Drag handler
  const onDrag = (event: MouseEvent) => {
    const { x, y } = toParentCoordinates(event, domElement.value)
    const { distanceX, distanceY } = dragState.value.distanceToAnchor
    const newRect: BoundingRect = translateAnchorTo(rect.value, dragState.value.action, { x: x - distanceX, y: y - distanceY })
    updateRect(newRect)
  }

  // Drag handles component
  const dragHandles = h(DragHandles, {
    parentSize: size,
    dragState,
    onDrag,
    onDragStarted,
    onDragStopped
  })

  return {
    ...draggable,
    dragHandles
  }
}



/**
 * Setup draggable and enforce constraints on the parent (move and resize are bound within the parent rect)
 */
export const useConstrainedDraggable = (domElement: Ref<HTMLElement>, initialPosition: Coordinates, initialSize: Size) => {
  const draggable = baseDraggable(domElement, initialPosition, initialSize)
  const { rect, size, dragState, onDragStarted, onDragStopped, updateRect } = draggable

  const onDrag = (event: MouseEvent) => {
    const { x, y } = toParentCoordinates(event, domElement.value)
    const { distanceX, distanceY } = dragState.value.distanceToAnchor
    let newRect: BoundingRect = translateAnchorTo(rect.value, dragState.value.action, { x: x - distanceX, y: y - distanceY })

    if(!domElement.value.parentNode) throw new Error('Unable to handle drag, no parent node')
    const parentNode: HTMLElement = domElement.value.parentNode as HTMLElement
    const parentRect: DOMRect = parentNode.getBoundingClientRect()

    if(dragState.value.action == DragAction.Move) {
      if(newRect.top < 0) newRect = translateRectTo(newRect, newRect.left, 0)
      if(newRect.left < 0) newRect = translateRectTo(newRect, 0, newRect.top)
      if(newRect.bottom > parentRect.height) newRect = translateRectTo(newRect, newRect.left, parentRect.height - size.value.height)
      if(newRect.right > parentRect.width) newRect = translateRectTo(newRect, parentRect.width - size.value.width, newRect.top)
    } else {
      if(newRect.top < 0) newRect = translateAnchorTo(newRect, dragState.value.action, { x: undefined, y: 0 })
      if(newRect.left < 0) newRect = translateAnchorTo(newRect, dragState.value.action, { x: 0, y: undefined })
      if(newRect.bottom > parentRect.height) newRect = translateAnchorTo(newRect, dragState.value.action, { x: undefined, y: parentRect.height })
      if(newRect.right > parentRect.width) newRect = translateAnchorTo(newRect, dragState.value.action, { x: parentRect.width, y: undefined })
    }

    updateRect(newRect)
  }

  const dragHandles = h(DragHandles, {
    parentSize: size,
    dragState,
    onDrag,
    onDragStarted,
    onDragStopped
  })

  return {
    ...draggable,
    dragHandles
  }
}
