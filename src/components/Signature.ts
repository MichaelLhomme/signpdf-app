// Dialog helper
import { useQuasar } from 'quasar'
import DialogSignature from './DialogSignature.vue'


// Helper for QDialog API, opens a signature dialog and returns a promise resolving on the signature data
export const useSignatureDialog = () => {
  const $q = useQuasar()
  return () => {
    return new Promise(resolve => {
      $q.dialog({
        component: DialogSignature // eslint-disable-line @typescript-eslint/no-unsafe-assignment
      })
        .onOk((sig: string) => resolve(sig))
        .onCancel(() => {;})
    })
  }
}


// Expose SignatureImage here for convenience
import SignatureImage from './SignatureImage.vue'
export { SignatureImage }


// Interface exposed by SignatureImage
export interface ISignatureComponent {
  image: HTMLImageElement
}


// Signature position
export interface Position {
  x: number,
  y: number
}


// Type for info props
export interface SignatureInfo {
  id: string
  initialPosition: Position
  dataUrl: string
  component: ISignatureComponent
}


// Helper to convert dataUrl to Uint8Array
export const dataUrlToUint8Array = (dataUrl: string): Uint8Array => {
  const bytes = atob(dataUrl.split(',')[1])
  return Uint8Array.from(bytes, (v) => v.charCodeAt(0))
}


// Test if a point is contained in a given DOMRect
const rectContainsPoint = (top: number, left: number, rect: DOMRect): boolean => {
  return (
    left > rect.left &&
    left < rect.right &&
    top > rect.top &&
    top < rect.bottom
  )
}


// Test if there is any intersection between two DOMRect
const rectIntersect = (aRect: DOMRect, bRect: DOMRect): boolean => {
  return (
    rectContainsPoint(aRect.top, aRect.left, bRect) ||
    rectContainsPoint(aRect.top, aRect.right, bRect) ||
    rectContainsPoint(aRect.bottom, aRect.left, bRect) ||
    rectContainsPoint(aRect.bottom, aRect.right, bRect)
  )
}


// Page intersection data
export interface Intersection {
  pageIndex: number,
  x: number,
  y: number,
  width: number,
  height: number,
  boxWidth: number,
  boxHeight: number,
  toPageWidthRatio: number
}


// Compute intersection data of a signature
export const getIntersections = (refElt: HTMLElement, elements: HTMLElement[]): Intersection[] => {
  const refBox = refElt.getBoundingClientRect()

  return elements
    .flatMap<Intersection>((e, idx) => {
      const box = e.getBoundingClientRect()
      if(!rectIntersect(refBox, box)) return []
      return [{
        pageIndex: idx,
        x: refBox.left - box.left,
        y: refBox.top - box.top,
        width: refBox.width,
        height: refBox.height,
        boxWidth: box.width,
        boxHeight: box.height,
        toPageWidthRatio: refBox.width / box.width
      }]
    })
}
